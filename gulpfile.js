const {  src, dest, watch, series } = require('gulp');
var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const fileinclude = require('gulp-file-include');
const browsersync = require('browser-sync').create();

// Sass Task
function scssTask(){
  return src('./public/src/scss/*.scss', { sourcemaps: true })
    .pipe(sass())
    .pipe(postcss([cssnano()]))
    .pipe(dest('./public/css', { sourcemaps: '.' }));
}

// JavaScript Task
function jsTask(){
  return src('./public/src/js/*.js', { sourcemaps: true })
    .pipe(terser())
    .pipe(dest('./public/js', { sourcemaps: '.' }));
}


// Browsersync Tasks
function browsersyncServe(cb){
  browsersync.init({
    server: {
      baseDir: '.'
    }
  });
  cb();
}

function browsersyncReload(cb){
  browsersync.reload();
  cb();
}

// Watch Task
function watchTask(){
  watch([
    './public/*.html',
    './public/frontendHTML/*.html',
    './public/common/*.html'], series(includeHTML,includeFrontendHTML, browsersyncReload));
  watch([
    './public/src/scss/*.scss',
    './public/src/scss/**/*.scss',
    './public/src/**/*.js'
  ], series(scssTask, jsTask, browsersyncReload));
}


function includeHTML(){
  return gulp.src([
    './public/*.html'
    ])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('.'));
}

function includeFrontendHTML(){
  return gulp.src([
    './public/frontendHTML/*.html'
    ])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./frontendHTML'));
}
// Default Gulp task
exports.default = series(
  scssTask,
  jsTask,
  browsersyncServe,
  includeHTML,
  includeFrontendHTML,
  watchTask,
  
);